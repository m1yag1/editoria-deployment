# Editoria Deployment

An ansible playbook used to deploy an editoria-vanilla demo instance to an ubuntu 18.04 server or locally with Vagrant.

## Why does this exist?

The playbook overwrites some parts of the editoria-vanilla project with minimal changes to the source code itself. We wanted a way to deploy the project to a demo environment for evaluation that didn't require forking, changing code that wasn't in source control, and allowed for inserting our own environmental variables. To accomplish this we overwrite the following files:

* [editoria-vanilla/docker-compose.demo.yml.j2](./templates/opt/editoria/docker-compose.demo.yml.j2)
* [editoria-vanilla/Dockerfile](./templates/opt/editoria/Dockerfile.j2)

The `editoria-vanilla/docker-compose.demo.yml` exists in the [./templates/opt/editoria/docker-compose.demo.yml.j2](./templates/opt/editoria/docker-compose.demo.yml.j2) as a template file used by Ansible to insert values where template tags exist. The values are located in [./group_vars/all.yml](./group_vars/all.yml) and are not currently encrypted but they could be using ansible-vault. This is a great way to deploy to different environments and keep your secrets locked in source control all in one place.

The `editoria-vanilla/Dockerfile` uses a `node` user to download and build using `yarn` which caused issues because of user permissions and volume mounting. This deployment sets up the server with an `editoria` application user that is part of the `docker` group. This is necessary so we can run `docker-compose up` to start the services with minimal effort.  When the demo docker-compose file is executed it mounts an uploads directory causes permissions conflicts. The customized `editoria-vanilla/Dockerfile` fixes that issue by inserting a configurable editoria user/uid in place of the node user.

### TL;DR

This playbook is for you if you would like to deploy a demo instance of editoria and not muck around too much with setup. You still need to install a few things and run a commands from the terminal.

## How can I use this playbook?

There are two methods to deploying a demo instance of editoria-vanilla

* Vagrant
* A server somewhere in the internets

You'll need to setup your local machine with Ansible for both options.

## Installing Ansible

> Note: This project assumes you'll be executing deployment commands from OSX. You'll need to adapt the installation steps based on your setup.

You can install Ansible using `brew`:

```bash
brew install ansible
```

If you have Python and pip installed you can also install Ansible via pip.

```bash
pip install ansible
```

### Downloading the roles from ansible galaxy

This playbook uses a couple roles out in the source code universe b/c we'd rather not have to re-invent the wheel.

* Prepare the ansible playbook by installing the requirements:

```bash
ansible-galaxy install -r requirements.yml
```

* This command will install the roles it needs within the [./galaxy_roles/](./galaxy_roles) directory. The roles that are used are:
    * geerlingguy.docker - a role that installs docker and docker compose.
    * openstax.pip - a role that installs the latest version of pip for installing the pip package docker-compose. This is required for ansible to run `docker-compose` commands.

## Deploying with Vagrant

This option is great for running things locally and not having to worry about yarn, setting up the db, or any application specific commands. However, if you want to share an instance with your teammates you'll want to follow the section on how to deploy to a server.

### Prerequisites

* Vagrant
* VirtualBox

### Deployment guide for Vagrant

* Run the following command to start Vagrant and provision a vm based on the instructions in the `Vagrantfile` included in the root of the project.

```bash
vagrant up
```
* When the playbook completes it will have used docker-compose to run the services at the following private ip `10.0.10.11:5030`

* SSH into the VM

```bash
vagrant ssh
```
* Open your browser to http://10.0.10.11:5030 and view the login screen.
* Sign in with the admin user. `admin:password`

## Deploy to a server

